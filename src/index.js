import React from 'react';
import ReactDOM from 'react-dom';
import './styles/global.css'

ReactDOM.render(
    <h1 className='global-h1'>Welcome to React Webpack Babel Starter Kit</h1>,
  document.getElementById('root')
);
